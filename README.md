# MOL style guide

Based on Reddit/Airbnb's guides. 

Please have a look at these guides before you start hacking away. It's all pretty common sense stuff and hopefully not too prescriptive.

* [General Style Guidelines](general-style-guidelines/) - good ideas to follow
  in any language
* [Javascript](javascript/)
* [HTML](html/)
* [CSS](css/)
* [PHP](php/) 

