# MOL General Style Guidelines

## The Golden Rule

Follow the style of the file you're in. It's more important to be *contextually*
correct than it is to be *absolutely* correct.

## Git workflow

### Commits

Individual commits should be atomic changes to the codebase. Specifically:

* A commit should not leave the code in a broken state waiting on a later
  commit to fix it.
* Changes that aren't related should be split into separate commits. This
  must be taken with the previous rule in mind to help determine what's
  related or not.  Bugfixes that are prerequisites to your change but not
  the main point of the change should be made in separate commits that come
  first.


### Commit Messages

Follow the [git commit message standard](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html):

```
Capitalized, short (75 chars or less) summary

Write your commit message in the imperative: "Fix bug" and not "Fixed bug"
or "Fixes bug."  This convention matches up with commit messages generated
by commands like git merge and git revert.

```

### Make sure changes are rebased on latest master.

Ensure your code is up-to-date to prevent merge conflicts and make testing
easier.
