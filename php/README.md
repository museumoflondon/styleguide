# MOL PHP Style Guide

## General style 

* If using a framework, ensure addtional custom code matches style used in rest of framework
* For Concrete5 version 5.7+, follow these [guidelines](http://www.php-fig.org/psr/psr-2/)   
